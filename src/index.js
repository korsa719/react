import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import {BrowserRouter as Router} from 'react-router-dom'
import styles from './index.css'
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'

import {createStore, applyMiddleware} from 'redux'
import reducers from './reducers/index'
import {Provider} from 'react-redux'
//window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()  redux->devtools
//applyMiddleware(thunk)   thunk->post
const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(<Provider store={store}><Router><App /></Router></Provider>, document.getElementById('main'))
