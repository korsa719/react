import { combineReducers } from 'redux'
import posts from './posts'
import likes from './likes'
import contacts from './contacts'
import loader from './loader'
import albums from'./albums'
import {reducer as form} from'redux-form'

export default combineReducers({
    posts,
    likes,
    contacts,
    loader,
    form:form,
    albums
});
