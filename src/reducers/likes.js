import {ADD_LIKE} from "../constants/likes";
const initialState = ['Vlad', 'Vika', 'Mark', 'Katy'];


function likes (state = initialState, action) {
    if (action.type === ADD_LIKE) {
        return [
        ...state, 
        action.payload
        ];
    }

    return state
}

export default likes;
