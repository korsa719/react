const initialState = [
{unique:'0001', name: 'Mark', phone: '095-223-24-12'},
{unique:'0031',name: 'Stan', phone: '093-243-41-33'},
{unique:'5031',name: 'Eugen', phone: '095-555-61-77'},
{unique:'1021',name: 'Vlada', phone: '066-777-21-66'},
{unique:'2322',name: 'Katya', phone: '098-111-25-16'},
]

function name(some) {
    return some.substring(0,some.indexOf(','));

}


function contacts (state = initialState, action) {
    if(action.type==='ADD_CONTACT'&& action.payload!=''){

      return [
      ...state,
      {
      name:  name(action.payload),
      phone:action.payload.substring(action.payload.indexOf(',')+1)
      }];
    }
    return state
}

export default contacts;
