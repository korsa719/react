import {ADD_LIKE} from "../constants/likes";
import {stopLoading,startLoading} from "./loading-creators";

export const onAddLike = likeName => ({type:ADD_LIKE,payload:likeName});

export  const onHandlerClickAsync = name =>dispatch=>{
    dispatch(startLoading());
    setTimeout(()=>{
        dispatch(stopLoading());
        dispatch({type:ADD_LIKE,payload:name})
    },3000)
};