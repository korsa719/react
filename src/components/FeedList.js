import React from 'react'
import FeedItem from './FeedItem'
import {getPosts} from "../helpers";

class FeedList extends React.Component {
    render (){
        return (<div className="feed-list">
            {this.props.posts.map( post => <FeedItem handleItemClick={this.props.handleClick} key={post.id} post={post} />)}
        </div>)
    }
}

export default FeedList;
