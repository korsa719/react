import React from 'react';

import Album from './Album';
import OneAlbum from './OneAlbum';
import {Link, Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux'

class Api extends React.Component{

  componentDidMount () {
  fetch('http://localhost:8080/albums')
    .then(response => response.json())
    .then(albums => this.props.onAddAlbum(albums))
}
 getAlbumById = id => {return this.props.albums.find( album => album.id == id)} //возвращает альюлм по айди
   render(){
       const albumsList = this.props.albums.map((album,index) =>
           <Link to={'/albums/'+album.id} key={index}>{album.title}</Link>
       );
  const list = this.props.albums; // список альбомов
  const WrappedAlbum = function(props) {
            return <Album posts={list} />;
        };

       return (
           <div>
              <nav>
                {albumsList}
              </nav>
              <Switch>
                <Route exact path="/albums" component={WrappedAlbum}/>
                <Route path="/albums/:id"  render={(props)=> <OneAlbum   post={this.getAlbumById(props.match.params.id)}/>}/>
              </Switch>
           </div>
       )
   }
}


export default connect(
  state => ({
    albums: state.albums
  }),
  dispatch =>({
   onAddAlbum:(albumName)=>{
      dispatch({type:'ALBUMS_LOADED',payload:albumName});
    }

  })
)(Api);
