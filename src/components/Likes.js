import React from 'react'
import LikeItem from './LikeItem'
import {connect} from 'react-redux'
import {onAddLike,onHandlerClickAsync} from '../action/likes-creators';
import {bindActionCreators} from 'redux'
import Loader from './Loader'



class Likes extends React.Component {
   addLike(){
    this.props.onHandlerClickAsync(this.likeInput.value);
    this.likeInput.value=''
   }

    render() {
        const list =this.props.likes.map((likedName,index) =>
        <LikeItem key={index} name={likedName} />
        );
        return (
            <div>
                <Loader/>
                <input type="text" ref={(input)=>{this.likeInput=input}}/>
                <button onClick={this.addLike.bind(this)}>Add</button>
                <ul className="list"> {list}</ul>
            </div>
        )
    }
}
const  mapStateToProps = (state) =>{
    return {
        likes:state.likes
    }
};
/*const mapDispatchToProps = (dispatch)=>{
    return{
        onAddLike: likeName => dispatch(onAddLike(likeName))
    }
};*/
const mapDispatchToProps = dispatch => bindActionCreators({onAddLike,onHandlerClickAsync},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Likes);
