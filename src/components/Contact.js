import React from 'react';


class   Contact extends React.Component{
  render(){
    return(
      <div>
    {this.props.name.map((name,index)=>
      <div className="userNameAndPhone" key={index}>
        <h3 className="first">{name.name}</h3>
        <h3 className="second">{name.phone}</h3>
      </div>
    )}
    </div>
  )
  }

}
export default Contact
