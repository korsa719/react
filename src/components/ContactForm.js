import React from 'react'
import {Field,reduxForm} from 'redux-form'


const ContactForm = (props)=> {

    return(
        <form onSubmit={props.handleSubmit}>
            <div>
                <label htmlFor="login">Name</label>
                <Field name="login" component="input" type="text"/>
            </div>
            <div>
                <label htmlFor="password">Password</label>
                <Field name="password" component="input" type="password"/>
            </div>
            <button type="submit">login</button>
            <button type="button" onClick={props.reset}>clear</button>
        </form>
    )
}

export default reduxForm({form:'auth'})(ContactForm)