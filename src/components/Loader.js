import React from 'react'
import {connect} from 'react-redux'

const Loader = props => (props.isLoading ? <img className="loader" src="loader.gif"/> :'');

const mapToStateProps = state=>{
    return {
        isLoading:state.loader
    }
}
export default connect(mapToStateProps)(Loader)