import React from 'react';


const OneAlbum = props => {
let post = {id:'', title:'',cover:''};
post = props.post != null ? props.post : post;



    return (
       <div className="albumsCanvas" key={post.id}>
         <img className="albumsCanvas_img"  src={post.cover}/>
         <h1>{post.title}</h1>
       </div>
     )


}

export default OneAlbum;
