import React from 'react';
import ArticleListTitle from './ArticleListTitle'
import ArticleListAuthor from './ArticleListAuthor'
import ArticleListDesc from './ArticleListDesc'

class   ArticleList extends React.Component{
    render(){

      return(
          <div>
            {this.props.list.map((item,index)=>
              <div  className="block" key={index}>
                <ArticleListTitle title={item.title} />
                <ArticleListAuthor author={item.author} />
                <ArticleListDesc desc={item.desc}/>
              </div>
            )}
          </div>
          )
    }
}
export default ArticleList;
