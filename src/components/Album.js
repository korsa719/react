import React from 'react';
import OneAlbum from './OneAlbum';

class Album extends React.Component{
  render(){
    return (
      <div>
      {this.props.posts.map( post => <OneAlbum  key={post.id} post={post} />)}
    </div>
  )
  }
}
export default Album;
