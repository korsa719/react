import React from 'react';
import {Link, Route, Switch} from 'react-router-dom'
import FeedItem from './FeedItem'
import FeedList from './FeedList'

// OPEN DEV TOOLS AND USE THIS COMMAND:
// window.localStorage.setItem('likes',[])

const likes = [
    {id: '1', author: 'origin', likes: '0'},
    {id: '2', author: 'dan_it', likes: '10'},
    {id: '3', author: 'placebo', likes: '2'},
    {id: '4', author: 'tigran', likes: '10000'}
]

const local =  window.localStorage.getItem('likes') ?  JSON.parse(window.localStorage.getItem('likes')) : likes;




class Feed extends React.Component {

    constructor(anyparams){
        super(anyparams)

        this.state ={
            likes: local
        }
    }

    // getPostById = function(id) {
    //     var likeObj = this.state.likes.find(function(like){
    //         return like.id === id;
    //     })
    //      return likeObj;
    // }

    getPostById = id => this.state.likes.find( like => like.id === id)

    handleLike = id => {
        const newLikes = [...this.state.likes];
        newLikes
            .find( like => like.id === id)
            .likes++;
        window.localStorage.setItem('likes', JSON.stringify(newLikes)) ;
        this.setState({likes: newLikes})

    }

    render (){
        const {match, history, location} = this.props;
        // const match = this.props.match;
        // const history = this.props.history;
        // const location = this.props.location;
        console.log(match, history, location)

        return (<div>
            <nav>
                <Link to="/feed/1">First</Link>
                <Link to="/feed/2">Second</Link>
                <Link to="/feed/3">Third</Link>
                <Link to="/feed/4">Tigra</Link>
            </nav>
            <Switch>
                {/*<Route exact path={'/feed'} component={FeedList}/>*/}
                {/*<Route path={`/feed/:post`} component={FeedItem}/>*/}
                <Route exact path={match.path} render={props => <FeedList handleClick={this.handleLike} posts={this.state.likes}/>}/>
                <Route path={`${match.path}/:id/`} render={(props)=> <FeedItem handleItemClick={this.handleLike} post={this.getPostById(props.match.params.id)}/>}/>
            </Switch>
        </div>)
    }
}





export default Feed
