import React from 'react';
import Home from './Home';
import About from './About';
import Likes from './Likes';
import Profile from './Profile';
import Articles from './Articles';
import Contacts from './Contacts';
import Feed from './Feed';
import Auth from './Auth';
import Api from './Api';
import Album from './Album';
import {Route,Link,Switch} from 'react-router-dom';



class App extends React.Component{

    render(){
        return (
            <div>
                <header>
                    <Link to="/">Root</Link>
                    <Link to="/articles">Articles</Link>
                    <Link to="/contacts">Contacts</Link>
                    <Link to="/about">About</Link>
                    <Link to="/likes">Likes</Link>
                    <Link to="/profile">Profile</Link>
                    <Link to="/feed">Feed</Link>
                    <Link to="/auth">Auth</Link>
                    <Link to="/albums">Api</Link>
                    

                </header>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/articles" component={Articles}/>
                    <Route exact path="/about" component={About}/>
                    <Route exact path="/likes" component={Likes}/>
                    <Route exact path="/profile" component={Profile}/>
                    <Route exact path="/contacts" component={Contacts}/>
                    <Route path="/feed" component={Feed}/>
                    <Route path="/auth" component={Auth}/>
                    <Route path="/albums" component={Api}/>
                   
                </Switch>
            </div>
        )
    }
}

export default App;
