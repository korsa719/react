import React from 'react';
import Contact from './Contact'
import {connect} from 'react-redux'


class Contacts extends React.Component{
  addContact(){
  this.props.onAddContact(this.contactInput.value);
  this.contactInput.value=''
  }
  render(){
  console.log(this.props.contacts)
    return (
      <div className="containerContacts">
        <input type="text" ref={(input)=>{this.contactInput=input}} placeholder="name,phone"/>
        <button onClick={this.addContact.bind(this)}>Add</button>
        <h1>Contact</h1>
        <Contact name={this.props.contacts}/>
    </div>
  )
  }

}
export default connect(
  state => ({
    contacts: state.contacts
  }),
  dispatch =>({
    onAddContact:(contactName)=>{
      dispatch({type:'ADD_CONTACT',payload:contactName});
    }
  })
)(Contacts);
