import React from 'react';
import {getPost} from "../helpers";
import PropTypes from 'prop-types';


const FeedItem = props => {
    const post = props.post;
console.log(post.id);
    return (
        <div className="feed_post">
            <header>{post.author}</header>

            <img className="post__photo" src={`/download_0${post.id}.jpeg`} alt=""/>
            <footer>Likes: {post.likes} <button onClick={()=> props.handleItemClick(post.id)}>Like it!</button></footer>
        </div>
    )

}


FeedItem.propTypes = {
    post: PropTypes.object.isRequired,
    handleItemClick: PropTypes.func.isRequired
};

export default FeedItem;
